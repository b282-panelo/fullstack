// fetch

// "fetch()" method returns a promise that resolves to a Response object 
fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
// .then((json) => console.log(json));
.then((data) => showPosts(data));

// ADD POST DATA
document.querySelector("#form-add-post").addEventListener("submit", (e) => {

	// Prevents the page from reloading
	// Prevents the default behavior of event 
	// submit - page reloading
	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts', {
		method: 'POST',
		headers: {'Content-Type': 'application/json;charset=UTF-8'},
		body: JSON.stringify({
			title: document.querySelector('#txt-title').value,
			body: document.querySelector('#txt-body').value,
			userId: 1
		})

	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Successfully added!');
		document.querySelector('#txt-title').value = null;
		document.querySelector('#txt-body').value = null;
	});
});


// RETRIEVE POSTS
const showPosts = (posts) => {
    let postEntries = "";

    posts.forEach((post) => {
 
        postEntries += `
            <div id ="post-${post.id}">
                <h3 id="post-title-${post.id}">${post.title}</h3>
                <p id="post-body-${post.id}">${post.body}</p>
                <button onClick="editPost('${post.id}')">Edit</button>
                <button onClick="deletePost('${post.id}')">Delete</button>
            </div>
        `
    });

    // To check what is stored in the properties of variables
    // console.log(postEntries);
    document.querySelector("#div-post-entries").innerHTML = postEntries;
};


// EDIT POST
const editPost = (id) => {

	let title = document.querySelector(`#post-title-${id}`).innerHTML;
	let body = document.querySelector(`#post-body-${id}`).innerHTML;

	document.querySelector("#txt-edit-id").value = id;
	document.querySelector("#txt-edit-title").value = title;
	document.querySelector("#txt-edit-body").value = body;

	document.querySelector('#btn-submit-update').removeAttribute('disabled');
};


// UPDATE POST
// Mini-activity


document.querySelector("#form-edit-post").addEventListener("submit", (e) => {

	e.preventDefault();

	fetch('https://jsonplaceholder.typicode.com/posts/1', {
		method: 'PUT',
		headers: {'Content-Type': 'application/json;charset=UTF-8'},
		body: JSON.stringify({
			id: document.querySelector('#txt-edit-id'),
			title: document.querySelector('#txt-edit-title').value,
			body: document.querySelector('#txt-edit-body').value,
			userId: 1
		})

	})
	.then((response) => response.json())
	.then((data) => {
		console.log(data);
		alert('Successfully updated!');
		document.querySelector('#txt-edit-id').value = null;
		document.querySelector('#txt-edit-title').value = null;
		document.querySelector('#txt-edit-body').value = null;

		document.querySelector('#btn-submit-update').setAttribute('disabled', true);
	});
});



