function countLetter(letter, sentence) {
    let result = 0;

    if (letter.length !== 1 || typeof letter !== 'string') {
        return undefined;
    }

    let count = 0;
    for (let i = 0; i < sentence.length; i++) {
        if (sentence[i] === letter) {
            count++;
        }
    }

    return count;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.

    
}


function isIsogram(text) {

    const lowerText = text.toLowerCase(); // Convert text to lowercase
    const encounteredLetters = {}; // Object to track encountered letters

    for (let i = 0; i < lowerText.length; i++) {
        const letter = lowerText[i];

        if (encounteredLetters[letter]) {
            return false; // The letter is repeating, return false
        }

        encounteredLetters[letter] = true; // Add the letter to the object
    }

    return true; // No repeating letters, return true (it's an isogram)

    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    // If the function finds a repeating letter, return false. Otherwise, return true.
 
}

console.log(isIsogram('word'));
console.log(isIsogram('hello'));
console.log(isIsogram('algorithm'));
console.log(isIsogram('javascript')); 


function purchase(age, price) {


    if (age < 13) {
        return undefined; 
    } else if (age >= 13 && age <= 21) {
        const discountedPrice = price * 0.8;
        return discountedPrice.toFixed(2); 
    } else if (age >= 65) {
        const discountedPrice = price * 0.8;
        return discountedPrice.toFixed(2);
    } else {
        return price.toFixed(2);
    }


    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
    
}

console.log(purchase(10, 100));
console.log(purchase(18, 50));
console.log(purchase(70, 80)); 
console.log(purchase(30, 45.99));

function findHotCategories(items) {

    // const hotCategories = [];

    // for (const item of items) {
    //     if (item.stocks === 0) {
    //         hotCategories.push(item.category);
    //     }
    // }

    // const uniqueHotCategories = Array.from(new Set(hotCategories));
    // return uniqueHotCategories;
    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    // console.log(findHotCategories(items));
}




function findFlyingVoters(candidateA, candidateB) {

    // const flyingVoters = candidateA.filter((voter) => candidateB.includes(voter));
    //     return flyingVoters;

    // Find voters who voted for both candidate A and candidate B.

    

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes. 
}

// // The passed values from the test are the following:
//     candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
//     candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

// console.log(findFlyingVoters(candidateA, candidateB));



module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};