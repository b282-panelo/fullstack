// console.log("Hello World!");


// Activity s47

// const txtFirstName = document.querySelector("#txt-first-name");
// const txtLastName = document.querySelector("#txt-last-name");
// const spanFullName = document.querySelector("#span-full-name");

// const updateFullName = () => {
//   const firstName = txtFirstName.value;
//   const lastName = txtLastName.value;
//   spanFullName.innerHTML = `${firstName} ${lastName}`;
//   console.log(firstName);
//   console.log(lastName);
// };

// txtFirstName.addEventListener("keyup", () => {
//   updateFullName();
// });

// txtLastName.addEventListener("keyup", () => {
//   updateFullName();
// });


const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");

const updateFullName = () => {
  const firstName = txtFirstName.value;
  const lastName = txtLastName.value;
  spanFullName.innerHTML = `${firstName} ${lastName}`;
  console.log(firstName);
  console.log(lastName);
};

const handleLastNameChange = () => {
  updateFullName();
};

txtFirstName.addEventListener("keyup", updateFullName);
txtLastName.addEventListener("keyup", updateFullName);
txtLastName.addEventListener("change", handleLastNameChange);
